#include <stdio.h>

struct student
{
  char fname[30];
  char subject[30];
  int marks;
};
int main()
{
  int i,total;
  printf("Enter the number of student records(min:5): \n");
  scanf("%d",&total);
  struct student stud[total];
  
  for(i=0; i<total; i++)
    {
      printf("Student %d\n",i+1);
      printf("Enter student first name :\n");
      scanf("%s", stud[i].fname);
      printf("Enter subject :\n");
      scanf("%s",stud[i].subject);
      printf("Enter marks :\n");
      scanf("%d", &stud[i].marks);
      
    }
  printf("----------Display stored data of students--------- \n");
  for(i=0; i<total; i++)
    {
      printf("Student %d\n",i+1);
      printf("Student Name : %s\n", stud[i].fname);
      printf("Subject : %s\n", stud[i].subject);
      printf("Marks : %d\n", stud[i].marks);
    }
  return 0;
}