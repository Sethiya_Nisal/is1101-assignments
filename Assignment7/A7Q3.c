#include <stdio.h>
 
int main()
{
  int m, n, p, q, c, d, k, sum = 0;
  int first[10][10], second[10][10], multiply[10][10], sum1[10][10];
 
  printf("Enter number of rows and columns of the first matrix:\n");
  scanf("%d%d", &m, &n);
  printf("Enter the elements of first matrix:\n");
 
  for (c = 0; c < m; c++)
    for (d = 0; d < n; d++)
      scanf("%d", &first[c][d]);
 
  printf("Enter number of rows and columns of the second matrix:\n");
  scanf("%d%d", &p, &q);
 if(m!=p || n!=q)
	{
		printf("Cannot add the matrices!");
		return;
	}
	else if(n!=p)
	{
		printf("Cannot multiply the matrices!");
		return;
	}
	else
  
  {

    printf("Enter the elements of second matrix:\n");
 
    for (c = 0; c < p; c++)
      for (d = 0; d < q; d++)
        scanf("%d", &second[c][d]);
 
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          sum = sum + first[c][k]*second[k][d];
        }
 
        multiply[c][d] = sum;
        sum = 0;
      }
    }
 
    printf("Multiplication of the matrices:\n");
 
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++)
        printf("%d\t", multiply[c][d]);
 
      printf("\n");
      }
       printf("Addition of the matrices:\n");
   
   for (c = 0; c < m; c++) {
      for (d = 0 ; d < n; d++) {
         sum1[c][d] = first[c][d] + second[c][d];
         printf("%d\t", sum1[c][d]);
      }
      printf("\n");
     
    }
  }
 
  return 0;
}
